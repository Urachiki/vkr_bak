import time
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import messagebox as mb
import socket
import threading


class State:
    def __init__(self):
        self.isPowerOn = 1
        self.progress = 50.0
        self.processTime = 0.0
        self.fileName = "/data.txt"
        self.update()

    def update(self):
        pass

    def toStr(self):
        str = ''
        str += '{:d}'.format(self.isPowerOn)
        str += " "
        str += '{:f}'.format(self.progress)
        str += " "
        str += '{:f}'.format(self.processTime)
        str += " "
        str += self.fileName
        print(str)
        return str


class Settings:
    def __init__(self):
        self.filePath = './'
        self.sendDelay = 1000
        self.optionsFilePath = './settings'
        self.ipAddress = ''
        self.readSettings()

    def saveSettings(self):
        f = open(self.optionsFilePath, 'w')
        f.write(self.filePath + '\n' + str(self.sendDelay))
        f.close()

    def readSettings(self):
        try:
            f = open(self.optionsFilePath, 'r')
            data = f.read()
            opt = data.split('\n')
            print(opt)
            self.filePath = opt[0]
            self.sendDelay = opt[1]
            self.ipAddress = opt[2]
            f.close()
        except FileNotFoundError:
            print("файл настроек не найден")
        print("state: ".join(self.filePath))
        print("send delay: " + self.sendDelay)
        print("ip to connect: " + self.ipAddress)


class Connection:
    def __init__(self):
        self.name = "one"
        self.sock = socket.socket()
        self.connectionStatus = 0
        self.getterMessages_threed = threading.Thread(target=self.messageListener)

    def getIP(self):
        return self.IP
        pass

    def setIP(self):
        return

    def connect(self, IP):
        if self.connectionStatus == 1:
            self.disconnect()
        try:

            self.IP = IP;
            self.sock.connect((IP, 9090))
            self.connectionStatus = 1
            self.getterMessages_threed.start()
        except Exception:
            print("Ошибка соединения с сервером")

    def disconnect(self):

        try:
            self.sock.close()

        except:
            print("Ошибка отключения от сервера")

    def sendState(self, state):
        try:
            # self.connect(self.IP)
            # self.sock = socket.socket()
            # self.sock.connect((self.IP, 9090))
            str = (self.name + " " + state.toStr()).encode()
            self.sock.send(str)
            # self.disconnect()
        except Exception:
            self.connectionStatus = 0
            print("Ошибка отправки данных")

    def getConnectionStatus(self):
        return self.connectionStatus

    def messageListener(self):
        while self.connectionStatus:
            rawData = self.sock.recv(1024).decode()
            print(rawData)
            data = rawData.split(" ")
            fileName = data[0]
            f = open(fileName, "wb")
            numPacks = int(data[1]) // 1024

            for i in range(numPacks):
                rawData = self.sock.recv(1024)
                print(rawData)
                f.write(rawData)
            f.close()

            print("получен файл: ", fileName)

    def reciveFile(self):
        pass


class Core:

    def initUI(self):
        self.buttonConnect = tk.Button(self.window, text="connect")
        self.buttonConnect.bind("<1>", self.buttonConnectClicked)
        self.buttonConnect['bg'] = 'yellow'
        self.buttonConnect.pack()

        self.buttonChooseFile = tk.Button(self.window, text="settingsFile")
        self.buttonChooseFile.bind("<1>", self.buttonChooseFileClicked)
        self.buttonChooseFile.pack()

        self.buttonStartSendData = tk.Button(self.window, text="start send data")
        self.buttonStartSendData.bind("<1>", self.buttonStartSendDataClicked)
        self.buttonStartSendData.pack();

        self.textInputIpServer = tk.Entry(self.window, width=40, font='Arial 10')
        self.textInputIpServer.pack()

    def __init__(self):
        self.settings = Settings()
        self.state = State()
        self.connection = Connection()

        self.window = tk.Tk()
        self.window.title = "main wondow"
        self.window.geometry('400x400')
        self.initUI()

        self.window.mainloop()

    def buttonConnectClicked(self, event):
        if (self.connection.getConnectionStatus() == 1):
            self.buttonConnect['bg'] = 'gray'
            self.buttonConnect.pack()
            self.connection.disconnect()
            return

        self.connection.connect(self.settings.ipAddress)

        if (self.connection.getConnectionStatus()):
            self.buttonConnect['bg'] = 'green'
            self.buttonConnect.pack()
        else:
            mb.showerror("Не удалось подключиться", "Не удалось подключиться")

    def buttonChooseFileClicked(self, event):
        filepath = fd.askopenfile()
        self.textInputIpServer.delete(0, tk.END)
        self.textInputIpServer.insert(0, filepath.name)
        self.settings.optionsFilePath = filepath.name
        self.settings.readSettings()

    def sendData(self):
        self.state.update()
        if (self.connection.getConnectionStatus() == 1):
            self.connection.sendState(self.state)
            self.window.after(10000, self.sendData)

    def buttonStartSendDataClicked(self, event):
        self.sendData()


core = Core()
