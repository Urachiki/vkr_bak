import socket
import sqlite3
import threading
import os


class User:
    def __init__(self):
        self.login = None
        self.psw = None
        self.id = None
        self.fileTransfer = None


class State:
    def __init__(self):
        self.isPowerOn = None
        self.progress = None
        self.processTime = None
        self.fileName = None
        self.idClient = None


class Client:
    def __init__(self):
        self.name = None
        self.ipAddress = None
        self.state = State()
        self.sendFrequency = 10
        self.id = None
        self.connectHandler = None


class DataBase:
    def __init__(self):
        self.cursor = None

    def addClient(self, client):
        self.dbConnect()
        self.cursor.execute("insert into clients(NAME,SENDFREQUENCY,IPADDRESS) values (?,?,?)",
                            (client.name, client.sendFrequency, client.ipAddress))
        self.connection.commit()
        self.cursor.execute("SELECT * FROM table WHERE id=(SELECT MAX(id) FROM table);")
        self.dbDisconnect()
        return self.cursor.fetchall()[0]

    def showClients(self):
        self.dbConnect()
        self.cursor.execute("select * from clients")
        print(self.cursor.fetchall())
        self.dbDisconnect()

    def addState(self, state=State()):
        self.dbConnect()
        self.cursor.execute("""insert into states(IDCNC,POWERON,PROGRESS,PROCESSTIME,FILENAME) 
        values (?,?,?,?,?)""", (state.idClient, state.isPowerOn, state.progress, state.processTime, state.fileName))
        self.connection.commit()
        self.dbDisconnect()

    def getStateByClientID(self, clientId):
        self.dbConnect()
        self.cursor.execute("SELECT * FROM states WHERE  (IDCNC = ?) and (ID = (SELECT MAX(ID) FROM states));",
                            (clientId,))
        print(self.cursor.fetchall())
        self.dbDisconnect()

    def getStateByClientName(self, name):
        try:
            self.dbConnect()
            self.cursor.execute("select * from clients where name = ?", name)
            rez = self.cursor.fetchall()
            self.cursor.execute("select * from states where idcnc = ?", rez[0][0])
            rez = self.cursor.fetchall()
            self.dbDisconnect()
            return rez
        except Exception:
            print("Ошибка получения статусов у клиента ", name)
            self.dbDisconnect()

    def converRawToUser(self, raw):
        users = []
        for rawUser in raw:
            user = User()
            user.id = int(rawUser[0])
            user.login = rawUser[1]
            user.psw = rawUser[2]
            user.fileTransfer = rawUser[3]

            users.append(user)
        return users

    def addUser(self, user):
        self.dbConnect()
        self.cursor.execute("""insert into users(USERNAME,PASSWORD,FILETRANSFER) 
                values (?,?,?)""",
                            (user.login, user.psw, user.fileTransfer))
        self.connection.commit()
        self.dbDisconnect()
        # TODO Проверка наличия пользователя в БД

    def getUsers(self):
        self.dbConnect()
        self.cursor.execute("select * from users")
        rez = self.cursor.fetchall()
        self.dbDisconnect()
        return self.converRawToUser(rez)

    def getUserByLogin(self, login):
        self.dbConnect()
        self.cursor.execute("select * from users where USERNAME = ?", (login,))
        rez = self.cursor.fetchall()
        self.dbDisconnect()
        return self.converRawToUser(rez)[0]

    def getClients(self):
        self.dbConnect()
        self.cursor.execute("select * from clients")
        clients = []
        i = 0
        for client in self.cursor.fetchall():
            clients.append(Client())

            clients[i].id = client[0]
            clients[i].name = client[1]
            clients[i].sendFrequency = client[2]
            clients[i].ipAddress = client[3]
            i += 1
        self.dbDisconnect()
        return clients

    def getCLientByName(self, name):
        clients = self.getClients()
        for client in clients:
            if (name == client.name):
                return client
        return None

    def dbConnect(self):
        try:
            self.connection = sqlite3.connect("mydatabase.db")
            self.cursor = self.connection.cursor()
        except Exception:
            print("Ошибка подключения к Базе данных\n", Exception)

    def dbInit(self):
        if (self.cursor == None):
            self.dbConnect()
        str = ""
        for line in open("initDB.sql", "r"):
            str += line
        try:
            self.cursor.executescript(str)
            self.connection.commit()
        except sqlite3.OperationalError:
            print("Ошибка создания базы данных, скорее всего она уже существует")
        except Exception:
            print("Ошибка создания базы данных, не понятно что с ней")

    def dbDisconnect(self):
        self.connection.close()


class Core:

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(('', 9090))
        self.sock.listen(5)
        self.sock.settimeout(10000)
        self.clients = []
        self.clientsIDs = []
        self.clientsHandlers = []
        self.dataBase = DataBase()
        self.dataBase.dbConnect()
        # self.clients = self.dataBase.getClients()
        print(self.clients)

        coreThread = threading.Thread(target=self.mainLoop)
        coreThread.start()

        # self.mainLoop()

    # TODO проверка правильности введенных данных
    def checkExistClient(self, name):
        client = self.dataBase.getCLientByName(name)
        if (client == None):
            return 0
        else:
            return client.id

    def sendFile(self, clientHandler, fileName):
        try:
            fileSize = os.path.getsize(fileName)
            clientHandler.send((fileName + " " + str(fileSize)).encode())
            f = open(fileName, "rb")
            l = f.read(1024)
            while (l):
                clientHandler.send(l)
                l = f.read(1024)
        except Exception:
            print("ошибка отправки файла")

    def messageHandling(self, clinetHandler, addr, rawData):
        data = rawData.split(" ")
        state = State()
        state.isPowerOn = int(data[1])
        state.progress = float(data[2])
        state.processTime = float(data[3])
        state.fileName = data[4]
        id = self.checkExistClient(data[0])

        client = Client()
        client.state = state
        client.state.idClient = id
        client.id = id
        client.connectHandler = clinetHandler
        client.ipAddress = addr[0]
        client.name = data[0]

        if (id == 0):
            client.id = self.dataBase.addClient(client)
            client.state.id = client.id
        if (client.id not in self.clientsIDs):
            self.clients.append(client)
            self.clientsIDs.append(client.id)
        if (clinetHandler not in self.clientsHandlers):
            self.clientsHandlers.append(clinetHandler)

        self.dataBase.addState(client.state)

    def onNewClient(self, clientSocket, addr):
        try:
            while True:
                if clientSocket not in self.clients:
                    print("New connection:", addr)
                    # self.sendFile(clientSocket, "./mydatabase.db")
                else:
                    print('Got connection from', addr)
                rawData = clientSocket.recv(1024).decode()
                self.messageHandling(clientSocket, addr, rawData)
                # clientSocket.sendall("МЫ получили ваше сообщение!!!".encode())
        except Exception:
            index = self.clientsHandlers.index(clientSocket)
            self.clientsHandlers.remove(clientSocket)
            self.clients.remove(self.clients[index])
            print("ОДин из сокетов сломался. причина не известна ((")

    def mainLoop(self):
        while True:
            # self.dataBase.getStateByClientID(3)
            clientHandler, addr = self.sock.accept()
            myThread = threading.Thread(target=self.onNewClient, args=(clientHandler, addr))
            myThread.start()

            # clientHandler.close()
