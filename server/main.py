
from flask import Flask
from flask import render_template, request, session,redirect, url_for

from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import *
from flask_wtf import FlaskForm
from core import *

site = Flask(__name__)
site.config['CSRF_ENABLED'] = True
# site.debug = True
site.config['SECRET_KEY'] = 'a really really really really long secret key'



class LoginForm(FlaskForm):
    login = StringField("login: ", validators=[DataRequired()])
    password = StringField("password: ", validators=[DataRequired()])



core = Core()

@site.route("/")
@site.route('/index')
def index():
    userName = 0
    if 'username' in session:
        userName = session['username']
    return render_template("index.html", userName = userName, clients = core.clients, loginPage=url_for('login'), logoutPage=url_for('logout'))

@site.route('/login/', methods=['post', 'get'])
def login():
    username = ''
    password = ''
    if request.method == 'POST':
        username = request.form.get('username')  # запрос к данным формы
        password = request.form.get('password')

    if username == 'root' and password == 'pass':
        message = "Correct username and password"
        session['username'] = username
        return  redirect(url_for('index'))
    else:
        message = "Wrong username or password"


    return render_template('login.html', message=message)


@site.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))












site.run()




